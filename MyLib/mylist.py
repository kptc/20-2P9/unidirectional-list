from MyLib.item import Item

class MyList:
    def __init__(self):
        self.head = None

    def append(self, value):
        if self.head is None:
            self.head = Item(value)
            return

        current_item = self.head
        while current_item.next is not None:
            current_item = current_item.next

        current_item.next = Item(value)